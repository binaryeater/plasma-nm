# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2014, 2015, 2017, 2019, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-22 02:10+0000\n"
"PO-Revision-Date: 2023-07-25 21:45+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 22.12.3\n"

#: openconnectauth.cpp:322
#, kde-format
msgid "Failed to initialize software token: %1"
msgstr "소프트웨어 토큰을 초기화할 수 없음: %1"

#: openconnectauth.cpp:375
#, kde-format
msgid "Contacting host, please wait…"
msgstr "호스트 연결 중, 잠시 기다려 주십시오…"

#: openconnectauth.cpp:695
#, kde-format
msgctxt "Verb, to proceed with login"
msgid "Login"
msgstr "로그인"

#: openconnectauth.cpp:757
#, kde-format
msgid ""
"Check failed for certificate from VPN server \"%1\".\n"
"Reason: %2\n"
"Accept it anyway?"
msgstr ""
"VPN 서버 \"%1\"의 인증서를 확인할 수 없습니다.\n"
"이유: %2\n"
"인증서를 수락하시겠습니까?"

#: openconnectauth.cpp:852
#, kde-format
msgid "Connection attempt was unsuccessful."
msgstr "연결 시도가 실패했습니다."

#. i18n: ectx: property (windowTitle), widget (QWidget, OpenconnectAuth)
#: openconnectauth.ui:26
#, kde-format
msgid "OpenConnect VPN Authentication"
msgstr "OpenConnect VPN 인증"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: openconnectauth.ui:55
#, kde-format
msgid "VPN Host"
msgstr "VPN 호스트"

#. i18n: ectx: property (toolTip), widget (QPushButton, btnConnect)
#: openconnectauth.ui:81
#, kde-format
msgid "Connect"
msgstr "연결"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoconnect)
#: openconnectauth.ui:102
#, kde-format
msgid "Automatically start connecting next time"
msgstr "다음에 자동으로 연결"

#. i18n: ectx: property (text), widget (QCheckBox, chkStorePasswords)
#: openconnectauth.ui:109
#, kde-format
msgid "Store passwords"
msgstr "암호 저장"

#. i18n: ectx: property (text), widget (QCheckBox, viewServerLog)
#: openconnectauth.ui:153
#, kde-format
msgid "View Log"
msgstr "기록 보기"

#. i18n: ectx: property (text), widget (QLabel, lblLogLevel)
#: openconnectauth.ui:163
#, kde-format
msgid "Log Level:"
msgstr "기록 단계:"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:174
#, kde-format
msgid "Error"
msgstr "오류"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:179
#, kde-format
msgid "Info"
msgstr "정보"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:184
#, kde-format
msgctxt "like in Debug log level"
msgid "Debug"
msgstr "디버그"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:189
#, kde-format
msgid "Trace"
msgstr "추적"

#. i18n: ectx: property (windowTitle), widget (QWidget, OpenconnectProp)
#: openconnectprop.ui:20
#, kde-format
msgid "OpenConnect Settings"
msgstr "OpenConnect 설정"

#. i18n: ectx: property (title), widget (QGroupBox, grp_general)
#: openconnectprop.ui:26
#, kde-format
msgctxt "like in General settings"
msgid "General"
msgstr "일반"

#. i18n: ectx: property (text), widget (QLabel, label_41)
#: openconnectprop.ui:38
#, kde-format
msgid "Gateway:"
msgstr "게이트웨이:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: openconnectprop.ui:51
#, kde-format
msgid "CA Certificate:"
msgstr "CA 인증서:"

#. i18n: ectx: property (filter), widget (KUrlRequester, leCaCertificate)
#. i18n: ectx: property (filter), widget (KUrlRequester, leUserCert)
#. i18n: ectx: property (filter), widget (KUrlRequester, leUserPrivateKey)
#: openconnectprop.ui:61 openconnectprop.ui:250 openconnectprop.ui:267
#, kde-format
msgid "*.pem *.crt *.key"
msgstr "*.pem *.crt *.key"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: openconnectprop.ui:68
#, kde-format
msgid "Proxy:"
msgstr "프록시:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: openconnectprop.ui:81
#, kde-format
msgid "User Agent:"
msgstr "사용자 에이전트:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: openconnectprop.ui:94
#, kde-format
msgid "CSD Wrapper Script:"
msgstr "CSD 래퍼 스크립트:"

#. i18n: ectx: property (text), widget (QCheckBox, chkAllowTrojan)
#: openconnectprop.ui:104
#, kde-format
msgid "Allow Cisco Secure Desktop trojan"
msgstr "Cisco Secure Desktop Trojan 허용"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: openconnectprop.ui:114
#, kde-format
msgid "VPN Protocol:"
msgstr "VPN 프로토콜:"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:131
#, kde-format
msgid "Cisco AnyConnect"
msgstr "Cisco AnyConnect"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:136
#, kde-format
msgid "Juniper Network Connect"
msgstr "Juniper Network Connect"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:141
#, kde-format
msgid "PAN Global Protect"
msgstr "PAN Global Protect"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:146
#, kde-format
msgid "Pulse Connect Secure"
msgstr "Pulse Connect Secure"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:151
#, kde-format
msgid "F5 BIG-IP SSL VPN"
msgstr "F5 BIG-IP SSL VPN"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:156
#, kde-format
msgid "Fortinet SSL VPN"
msgstr "Fortinet SSL VPN"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:161
#, kde-format
msgid "Array SSL VPN"
msgstr "Array SSL VPN"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: openconnectprop.ui:169
#, kde-format
msgid "Reported OS:"
msgstr "보고할 OS:"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:188
#, kde-format
msgid "GNU/Linux"
msgstr "GNU/Linux"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:193
#, kde-format
msgid "GNU/Linux 64-bit"
msgstr "GNU/Linux 64비트"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:198
#, kde-format
msgid "Windows"
msgstr "Windows"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:203
#, kde-format
msgid "Mac OS X"
msgstr "Mac OS X"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:208
#, kde-format
msgid "Android"
msgstr "안드로이드"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:213
#, kde-format
msgid "Apple iOS"
msgstr "애플 iOS"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: openconnectprop.ui:224
#, kde-format
msgid "Reported Version"
msgstr "보고할 버전"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: openconnectprop.ui:234
#, kde-format
msgid "Certificate Authentication"
msgstr "인증서 인증"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: openconnectprop.ui:240
#, kde-format
msgid "User Certificate:"
msgstr "사용자 인증서:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: openconnectprop.ui:257
#, kde-format
msgid "Private Key:"
msgstr "개인 키:"

#. i18n: ectx: property (text), widget (QCheckBox, chkUseFsid)
#: openconnectprop.ui:274
#, kde-format
msgid "Use FSID for key passphrase"
msgstr "키 암호에 FSID 사용"

#. i18n: ectx: property (text), widget (QCheckBox, preventInvalidCert)
#: openconnectprop.ui:281
#, kde-format
msgid "Prevent user from manually accepting invalid certificates"
msgstr "사용자가 수동으로 잘못된 인증서를 수락하는 것 방지"

#. i18n: ectx: property (text), widget (QPushButton, buTokens)
#: openconnectprop.ui:307
#, kde-format
msgid "Token Authentication"
msgstr "토큰 인증"

#. i18n: ectx: property (windowTitle), widget (QWidget, OpenConnectToken)
#: openconnecttoken.ui:14
#, kde-format
msgid "OpenConnect OTP Tokens"
msgstr "OpenConnect OTP 토큰"

#. i18n: ectx: property (title), widget (QGroupBox, gbToken)
#: openconnecttoken.ui:20
#, kde-format
msgid "Software Token Authentication"
msgstr "소프트웨어 토큰 인증"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: openconnecttoken.ui:26
#, kde-format
msgid "Token Mode:"
msgstr "토큰 모드:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: openconnecttoken.ui:43
#, kde-format
msgid "Token Secret:"
msgstr "토큰 비밀 정보:"

#~ msgid "&Show password"
#~ msgstr "암호 보이기(&S)"
