# Spanish translations for plasmanetworkmanagement_openconnectui.po package.
# Copyright (C) 2014 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2014.
# Eloy Cuadra <ecuadra@eloihr.net>, 2014, 2015, 2017, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasmanetworkmanagement_openconnectui\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-07-22 02:10+0000\n"
"PO-Revision-Date: 2023-07-22 04:43+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: openconnectauth.cpp:322
#, kde-format
msgid "Failed to initialize software token: %1"
msgstr "No se ha podido inicializar el elemento de software: %1"

#: openconnectauth.cpp:375
#, kde-format
msgid "Contacting host, please wait…"
msgstr "Conectando con la máquina. Espere, por favor..."

#: openconnectauth.cpp:695
#, kde-format
msgctxt "Verb, to proceed with login"
msgid "Login"
msgstr "Inicio de sesión"

#: openconnectauth.cpp:757
#, kde-format
msgid ""
"Check failed for certificate from VPN server \"%1\".\n"
"Reason: %2\n"
"Accept it anyway?"
msgstr ""
"Ha fallado la comprobación del certificado del servidor VNP «%1».\n"
"Motivo: %2\n"
"¿Aceptarlo de todas formas?"

#: openconnectauth.cpp:852
#, kde-format
msgid "Connection attempt was unsuccessful."
msgstr "El intento de conexión no ha tenido éxito."

#. i18n: ectx: property (windowTitle), widget (QWidget, OpenconnectAuth)
#: openconnectauth.ui:26
#, kde-format
msgid "OpenConnect VPN Authentication"
msgstr "Autenticación VPN de OpenConnect"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: openconnectauth.ui:55
#, kde-format
msgid "VPN Host"
msgstr "Máquina VPN"

#. i18n: ectx: property (toolTip), widget (QPushButton, btnConnect)
#: openconnectauth.ui:81
#, kde-format
msgid "Connect"
msgstr "Conectar"

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoconnect)
#: openconnectauth.ui:102
#, kde-format
msgid "Automatically start connecting next time"
msgstr "Iniciar conexión automática la próxima vez"

#. i18n: ectx: property (text), widget (QCheckBox, chkStorePasswords)
#: openconnectauth.ui:109
#, kde-format
msgid "Store passwords"
msgstr "Guardar contraseñas"

#. i18n: ectx: property (text), widget (QCheckBox, viewServerLog)
#: openconnectauth.ui:153
#, kde-format
msgid "View Log"
msgstr "Ver el registro"

#. i18n: ectx: property (text), widget (QLabel, lblLogLevel)
#: openconnectauth.ui:163
#, kde-format
msgid "Log Level:"
msgstr "Nivel de registro:"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:174
#, kde-format
msgid "Error"
msgstr "Error"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:179
#, kde-format
msgid "Info"
msgstr "Información"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:184
#, kde-format
msgctxt "like in Debug log level"
msgid "Debug"
msgstr "Depurar"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbLogLevel)
#: openconnectauth.ui:189
#, kde-format
msgid "Trace"
msgstr "Seguimiento"

#. i18n: ectx: property (windowTitle), widget (QWidget, OpenconnectProp)
#: openconnectprop.ui:20
#, kde-format
msgid "OpenConnect Settings"
msgstr "Preferencias de OpenConnect"

#. i18n: ectx: property (title), widget (QGroupBox, grp_general)
#: openconnectprop.ui:26
#, kde-format
msgctxt "like in General settings"
msgid "General"
msgstr "General"

#. i18n: ectx: property (text), widget (QLabel, label_41)
#: openconnectprop.ui:38
#, kde-format
msgid "Gateway:"
msgstr "Puerta de enlace:"

#. i18n: ectx: property (text), widget (QLabel, label)
#: openconnectprop.ui:51
#, kde-format
msgid "CA Certificate:"
msgstr "Certificado de la AC:"

#. i18n: ectx: property (filter), widget (KUrlRequester, leCaCertificate)
#. i18n: ectx: property (filter), widget (KUrlRequester, leUserCert)
#. i18n: ectx: property (filter), widget (KUrlRequester, leUserPrivateKey)
#: openconnectprop.ui:61 openconnectprop.ui:250 openconnectprop.ui:267
#, kde-format
msgid "*.pem *.crt *.key"
msgstr "*.pem *.crt *.key"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: openconnectprop.ui:68
#, kde-format
msgid "Proxy:"
msgstr "Proxy:"

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: openconnectprop.ui:81
#, kde-format
msgid "User Agent:"
msgstr "Agente de usuario:"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: openconnectprop.ui:94
#, kde-format
msgid "CSD Wrapper Script:"
msgstr "Script de envoltura CSD:"

#. i18n: ectx: property (text), widget (QCheckBox, chkAllowTrojan)
#: openconnectprop.ui:104
#, kde-format
msgid "Allow Cisco Secure Desktop trojan"
msgstr "Permitir el troyano de escritorio seguro de Cisco"

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: openconnectprop.ui:114
#, kde-format
msgid "VPN Protocol:"
msgstr "Protocolo VPN:"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:131
#, kde-format
msgid "Cisco AnyConnect"
msgstr "Cisco AnyConnect"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:136
#, kde-format
msgid "Juniper Network Connect"
msgstr "Juniper Network Connect"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:141
#, kde-format
msgid "PAN Global Protect"
msgstr "PAN GlobalProtect"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:146
#, kde-format
msgid "Pulse Connect Secure"
msgstr "Pulse Connect Secure"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:151
#, kde-format
msgid "F5 BIG-IP SSL VPN"
msgstr "VPN SSL de F5 BIG-IP"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:156
#, kde-format
msgid "Fortinet SSL VPN"
msgstr "VPN SSL de Fortinet"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbProtocol)
#: openconnectprop.ui:161
#, kde-format
msgid "Array SSL VPN"
msgstr "VPN SSL de Array"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: openconnectprop.ui:169
#, kde-format
msgid "Reported OS:"
msgstr "OS identificado:"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:188
#, kde-format
msgid "GNU/Linux"
msgstr "GNU/Linux"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:193
#, kde-format
msgid "GNU/Linux 64-bit"
msgstr "GNU/Linux de 64 bits"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:198
#, kde-format
msgid "Windows"
msgstr "Windows"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:203
#, kde-format
msgid "Mac OS X"
msgstr "Mac OS X"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:208
#, kde-format
msgid "Android"
msgstr "Android"

#. i18n: ectx: property (text), item, widget (QComboBox, cmbReportedOs)
#: openconnectprop.ui:213
#, kde-format
msgid "Apple iOS"
msgstr "Apple iOS"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: openconnectprop.ui:224
#, kde-format
msgid "Reported Version"
msgstr "Versión identificada"

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: openconnectprop.ui:234
#, kde-format
msgid "Certificate Authentication"
msgstr "Autenticación del certificado"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: openconnectprop.ui:240
#, kde-format
msgid "User Certificate:"
msgstr "Certificado del usuario:"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: openconnectprop.ui:257
#, kde-format
msgid "Private Key:"
msgstr "Clave privada:"

#. i18n: ectx: property (text), widget (QCheckBox, chkUseFsid)
#: openconnectprop.ui:274
#, kde-format
msgid "Use FSID for key passphrase"
msgstr "Usar FSID para la frase de contraseña"

#. i18n: ectx: property (text), widget (QCheckBox, preventInvalidCert)
#: openconnectprop.ui:281
#, kde-format
msgid "Prevent user from manually accepting invalid certificates"
msgstr "Impide que el usuario acepte certificados no válidos de forma manual"

#. i18n: ectx: property (text), widget (QPushButton, buTokens)
#: openconnectprop.ui:307
#, kde-format
msgid "Token Authentication"
msgstr "Autenticación del token"

#. i18n: ectx: property (windowTitle), widget (QWidget, OpenConnectToken)
#: openconnecttoken.ui:14
#, kde-format
msgid "OpenConnect OTP Tokens"
msgstr "Tokens OTP de OpenConnect"

#. i18n: ectx: property (title), widget (QGroupBox, gbToken)
#: openconnecttoken.ui:20
#, kde-format
msgid "Software Token Authentication"
msgstr "Autenticación del token por software"

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: openconnecttoken.ui:26
#, kde-format
msgid "Token Mode:"
msgstr "Modo del token:"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: openconnecttoken.ui:43
#, kde-format
msgid "Token Secret:"
msgstr "Credencial del token:"

#~ msgid "&Show password"
#~ msgstr "Mo&strar contraseña"
