# Translation of plasmanetworkmanagement-kcm.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: plasmanetworkmanagement-kcm\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-08 08:48+0000\n"
"PO-Revision-Date: 2017-10-30 23:08+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavianlatin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

# >> @title:window
#: kcm.cpp:284
#, fuzzy, kde-format
#| msgid "Import VPN Connection"
msgid "Failed to import VPN connection: %1"
msgstr "Uvoz VPN veze"

# well-spelled: моја_дељена_веза
#: kcm.cpp:354
#, kde-format
msgid "my_shared_connection"
msgstr "moja_deljena_veza"

# >> @title:window
#: kcm.cpp:426
#, kde-format
msgid "Export VPN Connection"
msgstr "Izvoz VPN veze"

#: kcm.cpp:446
#, kde-format
msgid "Do you want to save changes made to the connection '%1'?"
msgstr "Želite li da sačuvate izmene nad vezom „%1“?"

#: kcm.cpp:447
#, kde-format
msgctxt "@title:window"
msgid "Save Changes"
msgstr "Upisivanje izmena"

# >> @title:window
#: kcm.cpp:542
#, kde-format
msgid "Import VPN Connection"
msgstr "Uvoz VPN veze"

# >> @title:window
#: kcm.cpp:544
#, fuzzy, kde-format
#| msgid "Export VPN Connection"
msgid "VPN connections (%1)"
msgstr "Izvoz VPN veze"

#: kcm.cpp:547
#, kde-format
msgid "No file was provided"
msgstr ""

#: kcm.cpp:608
#, kde-format
msgid "Unknown VPN type"
msgstr ""

#: qml/AddConnectionDialog.qml:18
msgctxt "@title:window"
msgid "Choose a Connection Type"
msgstr "Izbor tipa veze"

#: qml/AddConnectionDialog.qml:165
msgid "Create"
msgstr "Napravi"

#: qml/AddConnectionDialog.qml:176
msgid "Cancel"
msgstr "Odustani"

#: qml/AddConnectionDialog.qml:193 qml/main.qml:177
msgid "Configuration"
msgstr ""

#: qml/ConfigurationDialog.qml:20
msgctxt "@title:window"
msgid "Configuration"
msgstr ""

#: qml/ConfigurationDialog.qml:35
msgid "General"
msgstr ""

#: qml/ConfigurationDialog.qml:43
msgid "Ask for PIN on modem detection"
msgstr ""

#: qml/ConfigurationDialog.qml:50
#, fuzzy
#| msgid "Export selected connection"
msgid "Show virtual connections"
msgstr "Izvezi izabranu vezu"

#: qml/ConfigurationDialog.qml:55
msgid "Hotspot"
msgstr ""

#: qml/ConfigurationDialog.qml:63
msgctxt "@label Hotspot name"
msgid "Name:"
msgstr ""

#: qml/ConfigurationDialog.qml:69
msgctxt "@label Hotspot password"
msgid "Password:"
msgstr ""

#: qml/ConfigurationDialog.qml:88
msgid "Hotspot name must not be empty"
msgstr ""

#: qml/ConfigurationDialog.qml:98
msgid ""
"Hotspot password must either be empty or consist of anywhere from 8 up to 64 "
"characters"
msgstr ""

#: qml/ConfigurationDialog.qml:99
msgid ""
"Hotspot password must either be empty or consist of one of the following:"
"<ul><li>Exactly 5 or 13 any characters</li><li>Exactly 10 or 26 hexadecimal "
"characters:<br/>abcdef, ABCDEF or 0-9</li></ul>"
msgstr ""

#: qml/ConnectionItem.qml:108
msgid "Connect"
msgstr "Poveži se"

#: qml/ConnectionItem.qml:108
msgid "Disconnect"
msgstr "Prekini vezu"

#: qml/ConnectionItem.qml:121
msgid "Delete"
msgstr "Obriši"

#: qml/ConnectionItem.qml:131
msgid "Export"
msgstr "Izvezi"

# >> @item
#: qml/ConnectionItem.qml:141
msgid "Connected"
msgstr "povezan"

# >> @item
#: qml/ConnectionItem.qml:143
msgid "Connecting"
msgstr "u povezivanju"

#: qml/main.qml:127
msgid "Add new connection"
msgstr "Dodaj novu vezu"

#: qml/main.qml:141
msgid "Remove selected connection"
msgstr "Ukloni izabranu vezu"

#: qml/main.qml:157
msgid "Export selected connection"
msgstr "Izvezi izabranu vezu"

#: qml/main.qml:205
msgctxt "@title:window"
msgid "Remove Connection"
msgstr "Uklanjanje veze"

#: qml/main.qml:206
msgid "Do you want to remove the connection '%1'?"
msgstr "Želite li zaista da uklonite vezu „%1“?"
